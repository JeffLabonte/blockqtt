# BlockQTT

[![pipeline status](https://gitlab.com/JeffLabonte/blockqtt/badges/master/pipeline.svg)](https://gitlab.com/JeffLabonte/blockqtt/commits/master) [![coverage report](https://gitlab.com/JeffLabonte/blockqtt/badges/master/coverage.svg)](https://gitlab.com/JeffLabonte/blockqtt/commits/master)


This project aims to create a more secure MQTT broker for the IoT devices connected to it.

I am planning to add the blockchain technology to help me achieve better security with the devices connected to the broker!