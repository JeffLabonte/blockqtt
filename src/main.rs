mod tcp_server;
mod web_socket_server;

fn main() {
    let response = tcp_server::init_server();

    match response {
        Ok(()) => println!("Win"),
        Err(err) => println!("{}", err)
    }
}
