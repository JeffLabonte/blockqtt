use std::net::{TcpListener};

pub struct WebSocketServer{
    magic_string: &'static str,
    host: &'static str,
    tcp_server: TcpListener
}

impl WebSocketServer{

    pub fn initialize(port: &str) -> WebSocketServer{
        let host = "127.0.0.1";
        WebSocketServer {
            host,
            magic_string: "258EAFA5-E914-47DA-95CA-C5AB0DC85B11",
            tcp_server: TcpListener::bind(format!("{}:{}", host, port)).unwrap()
        }
    }

}
