use std::net::{TcpListener};
use std::io;

pub fn init_server() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:4080")?;

    for stream in listener.incoming(){
        println!("{:?}", stream);
    }

    Ok(())
}
