mod web_socket_server;

#[cfg(test)]
mod tests{
    use super::web_socket_server::handshake::{ WebSocketServer };
    #[test]
    fn init_websocket(){
        let port: & str = "4080";
        let web_socket = WebSocketServer::initialize(port);
    }

    #[test]
    #[should_panic]
    fn init_websocket_diff_port(){
        let port = "4081";
        let web_socket = WebSocketServer::initialize(port);
        let web_socket_2 = WebSocketServer::initialize(port);
    }
}
